#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 12:38:30 2021

@author: David Oramas Muñoz
"""
import numpy as np
import dmd
import scipy.io
import matplotlib.pyplot as plt

mat = scipy.io.loadmat('Tensor_Re220p6_t2900_3d.mat')
A=mat["Tensor"]
t=np.arange(500)/2
dt=t[1]-t[0]

d=1
esvd=1e-2
edmd=1e-2

print("Tamaño original: {}".format(A.size))
print("Espacio original: {} MB".format(A.nbytes/1024**2))
print("")

#============ Desdoblar la matriz para probar con dmd sin tensor ==============

Ax=np.zeros((A.shape[0]*A.shape[1]*A.shape[2]*A.shape[3],A.shape[4]))
for i in range(A.shape[0]):
    c1=i*A.shape[1]*A.shape[2]*A.shape[3]
    for j in range(A.shape[1]):
        c2=j*A.shape[2]*A.shape[3]
        for k in range(A.shape[3]):
            Ax[c1+c2+k*A.shape[2]:c1+c2+(k+1)*A.shape[2],:]=A[i,j,:,k,:]
np.savetxt("cylinderuvwxyz.csv", Ax, delimiter=" ")

# B=dmd.unfold(A,4)
# Anew=dmd.fold(B,np.array(A.shape),4)

#============ Prueba de HOSVD =================================================
S,Ulist,slist=dmd.truncHOSVD(A,1e-2)
print("Tamaño svd: {}".format(S.size+Ulist[0].size+Ulist[1].size+Ulist[2].size+Ulist[3].size+Ulist[4].size))
print("Espacio svd: {} MB".format((S.nbytes+Ulist[0].nbytes+Ulist[1].nbytes+Ulist[2].nbytes+Ulist[3].nbytes+Ulist[4].nbytes)/1024**2))
Snew=S.copy()

for i in range(S.ndim):
    Snew=dmd.tensormatmul(Snew,Ulist[i],i)
print("Error svd: {}".format(np.linalg.norm(A-Snew)/np.linalg.norm(A)))
print("")

#============ Prueba de tensorHODMD ===========================================

u,a,mu=dmd.tensorHODMD(A,d,esvd,edmd)
u1,a1,mu1=dmd.tensorHODMD(A,1,esvd,edmd)
dl=np.log(mu).real/dt #r'
om=np.log(mu).imag/dt #r'
dl1=np.log(mu1).real/dt #r'
om1=np.log(mu1).imag/dt #r'
print("Tamaño dmd: {}".format(u.size+mu.size))
print("Espacio dmd: {} MB".format((u.nbytes+mu.nbytes)/1024**2))
Admd=dmd.remakeTens(u,t,mu)
print("Error dmd: {}".format(np.linalg.norm(A-Admd.real)/np.linalg.norm(A)))

#Frecuencias y factores de crecimiento
fig3,ax3=plt.subplots(figsize=(9,7))
ax3.set_title("Espectro estela del cilindro")
ax3.set_yscale('log')
ax3.scatter(om,a/np.amax(a),marker='o',c='none',edgecolors='black')
ax3.scatter(om1,a1/np.amax(a1),marker='+',c='b',edgecolors='b')
ax3.set_ylabel('Amplitudes a/max(a)',fontsize=14)
ax3.set_xlabel(r'$\omega$',fontsize=14)


#Cálculo del mejor orden:
#dlist=np.arange(1,201,20)
#Rmslist=np.zeros((3,dlist.size))
#id2=np.arange(t.size/2,dtype=np.int64)
#id4=np.arange(t.size/4,dtype=np.int64)
##Maxlist=np.zeros(dlist.size)
#normA=np.linalg.norm(A)
#for i in range(dlist.size):
#    print(i)
#    ul,al,mu=dmd.tensorHODMD(A,t,dlist[i],esvd,edmd)
#    ul2,al2,mu2=dmd.tensorHODMD(A[:,:,:,:,id2*2],t[id2*2],dlist[i],esvd,edmd)
#    ul4,al4,mu4=dmd.tensorHODMD(A[:,:,:,:,id4*4],t[id4*4],dlist[i],esvd,edmd)
#    Vl=dmd.remakeTens(ul,t,mu)
#    Vl2=dmd.remakeTens(ul2,t,mu2)
#    Vl4=dmd.remakeTens(ul4,t,mu4)
#    Rmslist[0,i]=np.linalg.norm(A-Vl.real)/normA
#    Rmslist[1,i]=np.linalg.norm(A-Vl2.real)/normA
#    Rmslist[2,i]=np.linalg.norm(A-Vl4.real)/normA
##    Rmslist[i],Maxlist[i]=dmd.error(snapshots,Vl[0])
#    
##print('Mejor orden: {}'.format(dlist[np.argwhere(Rmslist==np.amin(Rmslist))[0,0]]))
#    
#fig2,ax2=plt.subplots(figsize=(9,7))
#ax2.set_title("Estela del cilindro")
#ax2.set_yscale('log')
#ax2.plot(dlist,Rmslist[0],label='error RMS todas')
#ax2.plot(dlist,Rmslist[1],label='error RMS cada 2')
#ax2.plot(dlist,Rmslist[2],label='error RMS cada 4')
##ax2.plot(dlist,Maxlist,label='error máximo')
#ax2.legend()
#ax2.set_xlabel('Orden del dmd')
#ax2.set_ylabel('Error relativo')

#print(A[0,:,20,2,00])

fig4,ax4=plt.subplots(figsize=(9,7))
ax4.set_title("Corte")
#ax4.set_yscale('log')
ax4.plot(range(100),A[0,20,20,2,:100].real,label='original')#,marker='o',c='none',edgecolors='black')
ax4.plot(range(100),Admd[0,20,20,2,:100].real,label='reconstruccion')#,marker='+',c='black',edgecolors='black')
#ax4.scatter(om1,a1/np.amax(a1),marker='+',c='b',edgecolors='b')
ax4.set_xlabel('t',fontsize=14)
ax4.set_ylabel(r'$v_x$',fontsize=14)
ax4.legend()