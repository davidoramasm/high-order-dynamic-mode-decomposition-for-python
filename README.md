# High Order Dynamic Mode Decomposition for Python

Simple implementation of the High Order Dynamic Mode Decomposition (HODMD) for python, with support for n-dimensional tensor arranged raw data via high order singular value decomposition (time must always be the last dimension).


## Description
- dmd.py: contains al the method functions.
- toymodel1.py: easy to use toy model for testing the hodmd.
- CylinderWake.py: launcher for testing tensor arranged data (data repository used can be found in https://data.mendeley.com/datasets/hw6nz3zkg3/1)

## Authors and acknowledgment
David Oramas Muñoz.

## License
Free to use.

## Project status
Created for my end of masters thesis, no longer in development.
