#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 15:12:34 2021

@author: David Oramas Muñoz
"""

import numpy as np
import numba as nb
#import matplotlib.pyplot as plt

def error(V,Vrec):
    '''errores relativos RMS y máximo''' 
    return np.linalg.norm(V-Vrec)/np.linalg.norm(V),np.linalg.norm(V-Vrec,np.inf)/np.linalg.norm(V,np.inf)

@nb.njit(cache=True)
def truncatedSVD(A,esvd):
    '''Descomposición en valores singulares truncada en esvd'''
    U,s,Wh=np.linalg.svd(A,full_matrices=False)
    n=0
    norm=np.linalg.norm(s)
    for i in range(s.size):
        if np.linalg.norm(s[i:])/norm<=esvd:
            break
        else:
            n+=1
    return U[:,:n],s[:n],Wh[:n,:]

def unfold(A,dim):
    '''Convierte un tensor en una matriz conservando las columnas en dim'''
    ax=np.arange(A.ndim)
    return np.reshape(np.moveaxis(A,ax,np.roll(ax,dim)),(A.shape[dim],A.size//A.shape[dim]))

def fold(B,shape,dim):
    '''Operación contraria a unfold'''
    ax=np.arange(shape.size)
    shape=np.roll(shape,-dim)
    A=np.reshape(B,shape)
    return np.moveaxis(A,ax,np.roll(ax,-dim))

def tensormatmul(S,U,dim):
    '''Producto interior de un tensor por una matriz en dim'''
    shape=np.array(S.shape)
    shape[dim]=U.shape[0]
    return fold(U@unfold(S,dim),shape,dim)

def truncHOSVD(A,esvd):
    '''Descomposición en valores singulares para tensores, truncada en esvd'''
    Ulist=[]
    slist=[]
    S=A.copy()
    for i in range(A.ndim):
        [U,s,Vh]=truncatedSVD(unfold(A,i),esvd/np.sqrt(A.ndim))
        Ulist.append(U)
        S=tensormatmul(S,U.T,i)
    for i in range(A.ndim):
        s=np.zeros(S.shape[0])
        ax=np.arange(A.ndim)
        for j in range(S.shape[0]):
            s[j]=np.linalg.norm(S[j])
        slist.append(s)
        S=np.moveaxis(S,ax,np.roll(ax,1))
    return S,Ulist,slist

def dmd1(V,esvd,edmd):
    '''Descomposición modal dinámica de primer orden:
        Argumentos de entrada:
            -V (IxJ): snaphots temporales en los instantes t.
            -t (J): vector de instantes temporales.
            -esvd: umbral de truncamiento para descomposición en valores singulares.
            -edmd: umbral de truncamiento para amplitudes de modos.
        Argumentos de salida:
            -u (Ixn): matriz de modos (columnas) ordenada de mayor a menor amplitud, ya escaladas.
            -areal (n): vector de amplitudes (ordenadas) utilizadas para escalar u.
            -delta (n): vector de ratios de crecimiento.
            -omega (n): vector de frecuencias.'''
#    dt=t[1]-t[0]
    
    #Snapshots reducidas
    U,s,Wh=truncatedSVD(V,esvd) #I*r', r'*r', r'*J
#    Vvir=np.conj(U[:,:n].T)@V
#    Vvir=np.diag(s[:n])@Wh[:n,:] #r'*J
#    Vvir=s[:n]*Wh[:n,:]
    Vvir=np.diag(s)@Wh #r'*J
    
    #Reconstrucción matriz Koopman
    Uvir,svir,Wvirh=np.linalg.svd(Vvir[:,:-1],full_matrices=False) #r'*r', r'*r', r'*(J-1)
    Rvir=Vvir[:,1:]@Wvirh.conj().T@np.diag(svir**-1)@Uvir.conj().T #r'*r'
    eigval,eigvec=np.linalg.eig(Rvir) #r'*r'
    
    #Frecuencias y crecimientos
#    delta=np.log(eigval).real/dt #r'
#    omega=np.log(eigval).imag/dt #r'
    
    #Amplitudes
    A=np.zeros((eigvec.shape[0]*Vvir.shape[1],eigvec.shape[1])) #(r'*J)*r'
    b=np.zeros(eigvec.shape[0]*Vvir.shape[1])#(r'*J)*1
    for i in range(Vvir.shape[1]):
        A[i*eigvec.shape[0]:(i+1)*eigvec.shape[0],:]=eigvec@np.diag(eigval**i)
        b[i*eigvec.shape[0]:(i+1)*eigvec.shape[0]]=Vvir[:,i]
        
    Ua,sa,Wa=np.linalg.svd(A,full_matrices=False) #(r'*J)*r', r'*r', r'*r'
    a=Wa.conj().T@np.diag(sa**-1)@Ua.conj().T@b #r'
    
    #Modos
    uvir=eigvec@np.diag(a) #r'*r'
#    u=U[:,:n]@uvir #I*r'
    u=U@uvir #I*r'
#    areal=np.zeros(a.size) #Amplitudes reales
#    for i in range(u.shape[1]):
#        areal[i]=np.linalg.norm(u[:,i])/np.sqrt(V.shape[1])
    areal=np.linalg.norm(u,axis=0)/np.sqrt(V.shape[0])
    
    idx=np.flip(np.argsort(areal))
    u=u[:,idx]
    areal=areal[idx]
    eigval=eigval[idx]
#    delta=delta[idx]
#    omega=omega[idx]
    #Filtrando los importantes
    mask=(areal/areal[0])>edmd
    
    
    return u[:,mask],areal[mask],eigval[mask]#,delta[mask],omega[mask]

#@nb.njit(cache=True)
def hodmd(V,d,esvd,edmd):
    '''Descomposición modal dinámica de orden d:
        Argumentos de entrada:
            -V (IxJ): snaphots temporales en los instantes t.
            -t (J): vector de instantes temporales.
            -d: orden de la descomposición modal dinámica (entero>=1).
            -esvd: umbral de truncamiento para descomposición en valores singulares.
            -edmd: umbral de truncamiento para amplitudes de modos.
        Argumentos de salida:
            -u (Ixn): matriz de modos (columnas) ordenada de mayor a menor amplitud, ya escaladas.
            -areal (n): vector de amplitudes (ordenadas) utilizadas para escalar u.
            -delta (n): vector de ratios de crecimiento.
            -omega (n): vector de frecuencias.'''
#    dt=t[1]-t[0]
    
    #Snapshots reducidas
    U,s,Wh=truncatedSVD(V,esvd) #I*n, n*n, n*J
#    Vvir=np.conj(U[:,:n].T)@V
    Vvir=np.diag(s)@Wh #n*J
    print("Tamaño Vvir: ",Vvir.shape)
#    Vvir=s*Wh
    n=s.size
    
    #Snapshots reducidas y agrupadas
    Vdot=np.zeros((d*n,Vvir.shape[1]-d+1)) #(d*n)*(J-d+1)
    for j in range(d):
        Vdot[j*n:(j+1)*n,:]=Vvir[:,j:Vvir.shape[1]-d+j+1]
    print("Tamaño Vdot: ",Vdot.shape)
    #Snapshots reducidas, agrupadas y reducidas de nuevo
    Udot,sdot,Whdot=truncatedSVD(Vdot,esvd) #(d*n)*N, N*N, N*(J-d+1)
    Vvd=np.diag(sdot)@Whdot #N*(J-d+1)
    print("Tamaño Vvd: ",Vvd.shape)
    #Reconstrucción matriz Koopman
    Uvd,svd,Whvd=np.linalg.svd(Vvd[:,:-1],full_matrices=False) #N*r', r'*r', r'*(J-d)
    Rvd=Vvd[:,1:]@Whvd.conj().T@np.diag(svd**-1)@Uvd.conj().T #r'*r'
    eigval,eigvec=np.linalg.eig(Rvd) #r'*r'==N*N
    print("Tamaño Rvd: ",Rvd.shape)
    #Frecuencias y crecimientos
#    delta=np.log(eigval).real/dt #r'
#    omega=np.log(eigval).imag/dt #r'
    
    #Modos 
    q=(Udot@eigvec)[(d-1)*n:d*n,:] #Dando pasitos pa atrás n*N
    Uvir=q/np.linalg.norm(q,axis=0) #n*N
    print("Tamaño Uvir: ",Uvir.shape)
    #Amplitudes
    A=np.zeros((Uvir.shape[0]*Vvir.shape[1],Uvir.shape[1]),dtype=np.complex128) #(n*J)*N
    print("Tamaño A: ",A.shape)
    b=np.zeros(Uvir.shape[0]*Vvir.shape[1])#(n*J)*1
    for i in range(Vvir.shape[1]):
        A[i*Uvir.shape[0]:(i+1)*Uvir.shape[0],:]=Uvir@np.diag(eigval**i)
        b[i*Uvir.shape[0]:(i+1)*Uvir.shape[0]]=Vvir[:,i]
#    print(A[:Uvir.shape[0],:])
    Ua,sa,Wa=np.linalg.svd(A,full_matrices=False) #(n*J)*N, N*N, N*N
    a=Wa.conj().T@np.diag(sa**-1)@Ua.conj().T@b #N
    
#    print(eigval)
    #Modos
    uvir=Uvir@np.diag(a) #n*N
    u=U@uvir #I*N
    areal=np.linalg.norm(u,axis=0)/np.sqrt(V.shape[0])
    print("Tamaño ufull: ",u.shape)
    
    idx=np.flip(np.argsort(areal))
    u=u[:,idx]
    areal=areal[idx]
    eigval=eigval[idx]
#    delta=delta[idx]
#    omega=omega[idx]
    
    #Filtrando los importantes
    mask=(areal/areal[0])>edmd
    
    return u[:,mask],areal[mask],eigval[mask]#,delta[mask],omega[mask]

def tensorHODMD(V,d,esvd,edmd):
    '''Descomposición modal dinámica de orden d para tensores:
        Argumentos de entrada:
            -V (I1xI2x...xJ): snaphots temporales en los instantes t.
            -t (J): vector de instantes temporales.
            -d: orden de la descomposición modal dinámica (entero>=1).
            -esvd: umbral de truncamiento para descomposición en valores singulares.
            -edmd: umbral de truncamiento para amplitudes de modos.
        Argumentos de salida:
            -u (I1xI2x...xn): tensor de modos (columnas) ordenada de mayor a menor amplitud, ya escaladas.
            -areal (n): vector de amplitudes (ordenadas) utilizadas para escalar u.
            -delta (n): vector de ratios de crecimiento.
            -omega (n): vector de frecuencias.'''
    
    #Snapshots reducidas
    S,Vl,sl=truncHOSVD(V,esvd) #I*n, n*n, n*J
#    print(S.shape)
    Svir=S.copy()
    for i in range(S.ndim-1):
        Svir=tensormatmul(Svir,Vl[i],i)
    Svir=Svir/sl[-1]
#    Vl[-1]=(sl[-1]*Vl[-1]).T
    
    uvir,a,mu=hodmd((sl[-1]*Vl[-1]).T,d,esvd,edmd)
#    print(uvir.shape)
    u=tensormatmul(Svir,uvir.T,V.ndim-1)
    
    return u,a,mu



def remake(u,t,mu):
    '''Reconstruye los datos originales a partir de los resultados del dmd-d:
        Argumentos de entrada:
            -u (Ixn): matriz de modos (columnas).
            -t (J): vector de instantes temporales.
            -delta (n): vector de ratios de crecimiento.
            -omega (n): vector de frecuencias.
        Argumentos de salida:
            -vrec (IxJ): snaphots reconstruidas'''
            
    vrec=np.zeros((u.shape[0],t.size),dtype=np.complex128)
    for i in range(t.size):
        for j in range(u.shape[1]):
            vrec[:,i]+=u[:,j]*mu[j]**i#*np.exp((delta[j]+omega[j]*1j)*t[i])
    return vrec

#@nb.njit(cache=True)
def remakeTens(u,t,mu):
    '''Reconstruye los datos originales a partir de los resultados del dmd-d:
        Argumentos de entrada:
            -u (Ixn): matriz de modos (columnas).
            -t (J): vector de instantes temporales.
            -delta (n): vector de ratios de crecimiento.
            -omega (n): vector de frecuencias.
        Argumentos de salida:
            -vrec (IxJ): snaphots reconstruidas'''
            
    shape=np.array(u.shape,dtype=np.int32)
    shape[-1]=t.size
    vrec=np.zeros(tuple(shape),dtype=np.complex128)
    idx=[slice(None)]*shape.size
    for i in range(t.size):
        for j in range(u.shape[-1]):
            idx[-1]=i
            vrec[tuple(idx)]+=u.take(j,axis=-1)*mu[j]**i#*np.exp((delta[j]+omega[j]*1j)*t[i])
    return vrec