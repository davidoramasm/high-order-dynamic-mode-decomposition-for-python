#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 11:10:35 2021

@author: u5005269
"""

import numpy as np
import matplotlib.pyplot as plt
import dmd
    

def f(t):
    '''Funcion de la onda to guapa'''
    return 2/(2-np.cos(3*t)*np.cos(5*t))

K=1000
esvd=1e-12
edmd=1e-5
d=1

t=np.linspace(0,6,K)
dt=t[1]-t[0]
#(U,S,W)=np.linalg.svd(np.array([f(t)]))
snapshots=np.array([f(t)])


u,a,mu=dmd.dmd1(snapshots,esvd,edmd)
dl=np.log(mu).real/dt #r'
om=np.log(mu).imag/dt #r'
uh,ah,muh=dmd.hodmd(snapshots,d,esvd,edmd)
dlh=np.log(muh).real/dt #r'
omh=np.log(muh).imag/dt #r'

Vrec=dmd.remake(u,t,mu)
Vhdmd=dmd.remake(uh,t,muh)

Erms,Emax=dmd.error(snapshots,Vhdmd[0])
print('Error relativo RMS={}'.format(Erms))
print('Error relativo máximo={}'.format(Emax))

#Reconstrucción
fig1,ax1=plt.subplots(figsize=(9,5))

# ax1.plot(t,f(t),color='y',label='Analítico')
ax1.plot(t,Vrec[0].real,marker="^",markerfacecolor="w",markevery=50,label='Reconstrucción dmd1')
ax1.plot(t,Vhdmd[0].real,marker="o",color='r',markerfacecolor="w",markevery=10,label='Reconstrucción dmd{}'.format(d))
ax1.plot(t,f(t),color='g',label='Analítico')

ax1.tick_params(axis='both', which='major', labelsize=18)
ax1.tick_params(axis='both', which='minor', labelsize=16)
ax1.set_title("Reconstrucción",size=20)
# ax1.plot(t,Vhdmd[0].real,linestyle=(0, (5, 5)),color='r',label='Reconstrucción dmd{}'.format(d))
ax1.legend(prop={'size':14})
ax1.set_xlabel(r'$t$',size=20)
ax1.set_ylabel(r'$f(t)$',size=20)

fig1.savefig('ftreconst.eps', format='eps')

#Frecuencias y factores de crecimiento
fig2, ax2 = plt.subplots(2,1,sharex=True,figsize=(12, 10))

# fig3,ax3=plt.subplots(figsize=(12,5))
ax2[0].set_yscale('log')
ax2[0].scatter(omh,ah/np.amax(ah),marker='o',c='none',edgecolors='black')
ax2[0].tick_params(axis='both', which='major', labelsize=18)
ax2[0].tick_params(axis='both', which='minor', labelsize=16)
ax2[0].set_title("Amplitudes según frecuencia",size=20)
ax2[0].set_ylabel(r'Amplitudes $\left|\alpha\right|/\max(\left|\alpha\right|)$',size=20)
# ax2[0].set_xlabel(r'$\omega$',size=20)
# fig2[0].savefig('ftampl.eps', format='eps')

# fig4,ax4=plt.subplots(figsize=(12,5))
ax2[1].scatter(omh,dlh,marker='o',c='none',edgecolors='black')
ax2[1].tick_params(axis='both', which='major', labelsize=18)
ax2[1].tick_params(axis='both', which='minor', labelsize=16)
ax2[1].set_title("Crecimientos según frecuencia",size=20)
ax2[1].set_ylabel(r'$\delta$',size=20)
ax2[1].set_xlabel(r'$\omega$',size=20)

fig2.savefig('ftspectre.eps', format='eps')

#Cálculo del mejor orden:
#dlist=np.arange(1,701)
#Rmslist=np.zeros(dlist.size)
#Maxlist=np.zeros(dlist.size)
#for i in range(dlist.size):
#    ul,al,dll,oml=dmd.hodmd(snapshots,t,dlist[i],esvd,edmd)
#    Vl=dmd.remake(ul,t,dll,oml)
#    Rmslist[i],Maxlist[i]=dmd.error(snapshots,Vl[0])
#    
#print('Mejor orden: {}'.format(dlist[np.argwhere(Rmslist==np.amin(Rmslist))[0,0]]))
#    
#fig2,ax2=plt.subplots(figsize=(9,7))
#ax2.set_title("Toy model 1")
#ax2.set_yscale('log')
#ax2.plot(dlist,Rmslist,label='error RMS')
#ax2.plot(dlist,Maxlist,label='error máximo')
#ax2.legend()
#ax2.set_xlabel('Orden del dmd')
#ax2.set_ylabel('Error relativo')
